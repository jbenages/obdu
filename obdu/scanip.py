import nmap
from pprint import pprint
import datetime,jsbeautifier,json
from threading import Thread

class ScanIP():
    ip = ""
    ports = {}
    options = {}
    results = {}
    threads = {}
    command = {}
    consoleLogOut = None

    def __init__(self, ip, portsProto, optionsProto, consoleLogOut):
        self.ip = ip
        self.ports = portsProto
        self.options = optionsProto
        for proto in self.ports:
          self.threads[proto] = ""
          self.command[proto] = ""
          self.results[proto] = {"json":"","csv":""}
        self.scanner = nmap.PortScanner()
        self.consoleLogOut = consoleLogOut

    def scanIP(self,ip,proto,ports,options):
        self.scanner.scan(ip, ports, options)
        self.results[proto]["json"] = self.scanner._scan_result
        self.results[proto]["csv"] = self.scanner.csv()
        self.command[proto] = self.scanner.command_line()

    def scan(self):
        for proto in self.ports:
            if self.ports[proto] != None:
              self.threads[proto] = Thread(target=self.scanIP, args=(self.ip, proto, self.ports[proto], self.options[proto]))
              self.threads[proto].start()

        progressBarChar = ["-","\\","|","/"]
        progressBarCharCount = 0

        while not len(self.threads) == 0:
            if progressBarCharCount == len(progressBarChar):
                progressBarCharCount = 0
            self.consoleLogOut.output(' IP '+self.ip+' Proto '+(" and ".join(list(self.threads)))+' scan running['+progressBarChar[progressBarCharCount]+']','waiting','\r')
            progressBarCharCount = progressBarCharCount + 1
            for proto in list(self.threads):
                self.threads[proto].join(timeout=0.5)
                if not self.threads[proto].is_alive():
                    self.threads.pop(proto, None)
                    print(' '*60,end='\r')
                    self.consoleLogOut.output(' IP '+self.ip+' Scan '+proto+' finish.','done')

    def beautifyJson(self,jsonRaw):
        opts = jsbeautifier.default_options()
        opts.indent_size = 2
        return jsbeautifier.beautify(json.dumps(jsonRaw), opts)
   
    def createFile(self,path,rawData):
        with open(path, 'a+') as f:
            f.write(rawData)

    def existsResults(self):
        resultsExist = False
        for proto in self.results.keys():
            if self.ip in self.results[proto]["json"]["scan"]:
                resultsExist = True
        if not resultsExist:
            self.consoleLogOut.output(" Not results for IP:"+self.ip, "error")

        return resultsExist

    def storeResults(self,nmapFolderPath):
        dateFinishScan = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        for proto in self.results.keys():
            for output in self.results[proto].keys():
                rawData = self.results[proto][output]
                if output == "json":
                    rawData = self.beautifyJson(self.results[proto][output])
                self.createFile(nmapFolderPath+"/nmapScan-"+proto+"-"+dateFinishScan+"."+output, rawData)
                self.consoleLogOut.output(" File created: "+nmapFolderPath+"/nmapScan-"+proto+"-"+dateFinishScan+"."+output, "info")
    
    def generateAliases(self,aliases):
        for proto in self.results.keys():
           if proto in self.results[proto]["json"]["scan"][self.ip]:
               for port in self.results[proto]["json"]["scan"][self.ip][proto].keys():
                   self.results[proto]["json"]["scan"][self.ip][proto][port]["alias"] = []
                   if self.results[proto]["json"]["scan"][self.ip][proto][port]["state"].find("filtered") == -1:
                       for aliase in aliases.keys():
                           if 'name' in aliases[aliase]:
                               for subAlias in aliases[aliase]['name']:
                                   if self.results[proto]["json"]["scan"][self.ip][proto][port]["name"] == subAlias:
                                       self.results[proto]["json"]["scan"][self.ip][proto][port]["alias"].append(aliase)
                           if 'script' in aliases[aliase] and 'script' in self.results[proto]["json"]["scan"][self.ip][proto][port]:
                               for scriptAlias in aliases[aliase]['script']:
                                   for scriptContent in self.results[proto]["json"]["scan"][self.ip][proto][port]['script']:
                                       if scriptAlias.lower() in self.results[proto]["json"]["scan"][self.ip][proto][port]['script'][scriptContent].lower():
                                           self.results[proto]["json"]["scan"][self.ip][proto][port]["alias"].append(aliase)
                       if len(self.results[proto]["json"]["scan"][self.ip][proto][port]["alias"]) == 0:
                           self.consoleLogOut.output(" IP:"+self.ip+" Port:"+proto+"/"+str(port)+" with name:"+self.results[proto]["json"]["scan"][self.ip][proto][port]["name"]+" alias not found.", "info")
                       else:
                           self.consoleLogOut.output(" IP:"+self.ip+" Port:"+proto+"/"+str(port)+" with name:"+self.results[proto]["json"]["scan"][self.ip][proto][port]["name"]+" set alias:"+",".join(self.results[proto]["json"]["scan"][self.ip][proto][port]["alias"]), "done")

    def getResults(self):
        return self.results
