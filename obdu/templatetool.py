import yaml,os
from jinja2 import Template

class TemplateTool():

    tools = []
    templates = []
    commandTemplate = ""

    def __init__(self, toolsPath, templatesPath, commandTemplate):
        self.tools = yaml.safe_load(open(toolsPath))
        self.templates = yaml.safe_load(open(templatesPath))
        self.commandTemplate = commandTemplate
    
    def checkFileExists(self,folderPath):
        return os.path.exists(folderPath)
    
    def createFile(self,path,rawData):
        created = False
        if not self.checkFileExists(path):
            created = True
            with open(path, 'a+') as f:
                f.write(rawData)
        return created

    def generateTemplateFiles(self, aliasTemplate, ip, domain, proto, port, path, stealthLevel, consoleLogOut, projectData):
        for tool in self.templates[aliasTemplate].keys():
            for nameCommand in self.templates[aliasTemplate][tool]:
                commandTemplate = Template(self.tools[tool]["templates"][nameCommand]).render(ip=ip,domain=domain,proto=proto,port=port,options=self.tools[tool]["options"][stealthLevel],subProto=aliasTemplate)
                with open(self.commandTemplate) as f:
                    scriptTemplate = Template(f.read())
                    scriptRendered = scriptTemplate.render(tool=tool,templateName=nameCommand,command=commandTemplate)
                    projectData.createFolder(path+"/"+tool)
                    if self.createFile(path+"/"+tool+"/"+tool+"."+nameCommand+".sh",scriptRendered):
                        consoleLogOut.output(" IP:"+ip+" created template:"+path+"/"+tool+"/"+tool+"."+nameCommand+".sh","done")

    def generateTemplatesTools(self, projectData, resultsScan, domain, stealthLevel, consoleLogOut):
        for proto in resultsScan.keys():
            for ip in resultsScan[proto]["json"]["scan"]:
                self.generateTemplateFiles("general", ip, domain, "", "", projectData.projectPath+"/"+ip+"/", stealthLevel, consoleLogOut, projectData)
                if proto in resultsScan[proto]["json"]["scan"][ip]:
                    for port in resultsScan[proto]["json"]["scan"][ip][proto].keys():
                        for aliasTemplate in self.templates.keys():
                            for alias in resultsScan[proto]["json"]["scan"][ip][proto][port]['alias']:
                                if alias == aliasTemplate:
                                    self.generateTemplateFiles(aliasTemplate, ip, domain, proto, port, projectData.projectPath+"/"+ip+"/"+proto+"/"+str(port), stealthLevel, consoleLogOut, projectData)
