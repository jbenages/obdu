import os

class ProjectData():
    projectName = ""
    dataPath = ""
    projectPath = ""

    def __init__(self, dataPath, projectName):
        self.projectName = projectName
        self.dataPath = dataPath
        self.projectPath = self.dataPath + self.projectName

    def checkFolderExists(self,folderPath):
        return os.path.exists(folderPath)

    def createFolder(self, pathFolder):
        if not self.checkFolderExists(pathFolder):
            os.makedirs(pathFolder)
        return pathFolder

    def generateFolderStruct(self):
        return self.createFolder(self.dataPath + self.projectName)
    
    def generateFolderAsset(self,asset):
        return self.createFolder(self.dataPath + self.projectName + "/" + asset + "/nmap")
    
    def generateFolderPorts(self,asset,ports):
        for port in ports:
            pathPort = self.dataPath + self.projectName + "/" + asset + "/"+port["protocol"]+"/"+port["port"]
            if not self.checkFolderExists(pathPort):
                self.createFolder(pathPort)
