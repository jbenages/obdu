import sys,os,argparse,socket,yaml,json,jsbeautifier,csv,re
from obdu import *
from consolelog import *
from scanip import *
from projectdata import *
from templatetool import *

def beautifyJson(jsonRaw):
    opts = jsbeautifier.default_options()
    opts.indent_size = 2
    return jsbeautifier.beautify(json.dumps(jsonRaw), opts)

def validateIP(ip):
    socket.inet_aton(ip)

def splitAssets(assets):
    newAssets = {}
    assets = re.sub(' +', ' ', assets[0])
    for asset in assets.split(" "):
        assetSplited = asset.split(":")
        domain = ""
        ip = assetSplited[0].strip()
        validateIP(ip)
        if len(assetSplited) > 1:
            domain = assetSplited[1].strip()
        newAssets[ip] = domain

    return newAssets

def main():
    global consoleLogOut
    
    assets = ""
    selectedPorts = {
            "tcp":"",
            "udp":""
            }
    selectedOptions = {
            "tcp":"",
            "udp":""
            }

    parser = argparse.ArgumentParser(description='Autopwn Wordpress.')
    parser.add_argument(
            '-n',
            '--name', 
            nargs='?', 
            default="default/pentest", 
            help='Name of project to naming the folder structure, allowed subfolders.. Example: -n "client1/pentest2021"'
            )
    parser.add_argument(
            '-a',
            '--assets', 
            default=[], 
            nargs='+', 
            help='Assets, separated by space and with the main domain in semicolon. Example: -a "1.1.1.1:example.com 1.2.2.2:example2.com"'
            )
    parser.add_argument(
            '-A',
            '--ASSETS', 
            nargs='?', 
            type=argparse.FileType('r'), 
            help='Assets, separated by return and with the main domain in semicolon in file format. Example: -A "/path/to/file.assets"'
            )
    parser.add_argument(
            '-x',
            '--tcp-ports', 
            dest="tcpPorts",
            nargs='?', 
            help='Ports TCP to scan in nmap format. Example: -x "0-2048"'
            )
    parser.add_argument(
            '-u',
            '--udp-ports', 
            dest="udpPorts", 
            nargs='?', 
            help='Ports UDP to scan in nmap format. Example: -u "0-1024"'
            )
    parser.add_argument(
            '-p',
            '--path-folder', 
            dest="pathFolder", 
            nargs='?', 
            default="~/.obdu/", 
            help='Path to folder to store all information about pentest. Example: -p "./obdu"'
            )
    parser.add_argument(
            '-c',
            '--config', 
            nargs='?', 
            default="config/config.yml", 
            help='Path to configuration file. Example: -c "config.yml"'
            )
    parser.add_argument(
            '-s',
            '--stealth', 
            nargs='?', 
            default=0, 
            help='Stealth level. Example: -s1. Range: 0-1 '
            )
    parser.add_argument(
            '-v',
            '--verbose', 
            nargs='?', 
            default=0, 
            help='Verbose level Example: -v1. Range: 0-2. '
            )

    args = parser.parse_args()
    
    verbose = args.verbose
    consoleLogOut = ConsoleLog(int(verbose))
    configurationPath = args.config
    config = yaml.safe_load(open(os.path.abspath(sys.path[0])+"/"+configurationPath))
    aliases = yaml.safe_load(open(os.path.abspath(sys.path[0])+"/config/aliases.yml"))

    templateTool = TemplateTool(
            os.path.abspath(sys.path[0])+"/config/tools.yml",
            os.path.abspath(sys.path[0])+"/config/template.yml",
            os.path.abspath(sys.path[0])+"/config/template.j2"
            )
   
    dataPath = os.path.expanduser(args.pathFolder)
    stealth = int(args.stealth)
    projectName = args.name
    projectData = ProjectData(dataPath,projectName)
    consoleLogOut.output(" Project folder: " + projectData.generateFolderStruct(), "info")

    selectedOptions["udp"] = config["templates"]["nmap"]["udp"]["options"][stealth]
    selectedOptions["tcp"] = config["templates"]["nmap"]["tcp"]["options"][stealth]

    if args.tcpPorts != None:
        selectedPorts["tcp"] = args.tcpPorts
        config["templates"]["nmap"]["tcp"]["ports"] = args.tcpPorts
    else:
        selectedPorts["tcp"] = config["templates"]["nmap"]["tcp"]["ports"]
        
    if args.udpPorts != None:
        selectedPorts["udp"] = args.udpPorts
        config["templates"]["nmap"]["udp"]["ports"] = args.udpPorts
    else:
        selectedPorts["udp"] = config["templates"]["nmap"]["udp"]["ports"]

    if selectedPorts["tcp"] == "-1":
        selectedPorts.pop("tcp",None)
        selectedOptions.pop("tcp",None)
    if selectedPorts["udp"] == "-1":
        selectedPorts.pop("udp",None)
        selectedOptions.pop("udp", None)

    if args.assets != []:
        assets = args.assets
    elif args.ASSETS != None:
        assets = args.ASSETS
        assets = args.ASSETS.readlines()
    else:
        parser.print_help()
    assets = splitAssets(assets)
    consoleLogOut.output( " Target Assets:"+beautifyJson(assets), "info" )
    consoleLogOut.output( " Configuration:"+beautifyJson(config), "info" )
    for asset in assets:
        scanner = ScanIP(asset, selectedPorts, selectedOptions, consoleLogOut)
        scanner.scan()
        for proto in selectedPorts:
            if proto != None:
              consoleLogOut.output(" Nmap command: " + scanner.command[proto], "info")
        if scanner.existsResults():
            results = scanner.getResults()
            consoleLogOut.output(" Asset folder: " + projectData.generateFolderAsset(asset), "info")
            for proto in selectedPorts:
                csvResults = csv.DictReader(results[proto]["csv"].splitlines(), delimiter=';',skipinitialspace=True)
                rows = list(csvResults)
                if len(rows) == 0:
                    consoleLogOut.output(" IP "+asset+ " not find ports on proto: "+proto+".", "undone")
                else:
                    projectData.generateFolderPorts(asset,rows)
                    csvResults = csv.DictReader(results[proto]["csv"].splitlines(), delimiter=';',skipinitialspace=True)
                    for result in csvResults:
                        consoleLogOut.output(" IP "+asset+" find "+result["protocol"]+"/"+ result["port"], "done")
            scanner.generateAliases(aliases)
            scanner.storeResults(projectData.generateFolderAsset(asset))
            templateTool.generateTemplatesTools(projectData,results,assets[asset],stealth,consoleLogOut)

if __name__ == "__main__":
   main()

