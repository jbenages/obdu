# obdu

Pentesting and organizational tool to create your tools/commands template to store your results in folder structure in ordered mode.

## Description

### Folder struct

There are the steps that do the obdu to generate the folder struct with the bash scripts of tools created with templates that you generate previously:

1. Pass the minimum required arguments, assets lists and project name:
```bash
python3 obdu/obdu.py -a "127.0.0.1:localhost" -n "default/test/localhost/"
```
2. Obdu start scan with one asset, use 2 threads, one for TCP scan and other for UDP scan:
```bash
[*] IP 127.0.0.1 Proto tcp and udp scan running[|]
``` 
3. When finish obdu create the folder struct for each port in the asset folder:
```bash
[+] IP 127.0.0.1 Scan tcp finish.                       
[+] IP 127.0.0.1 Scan udp finish.
[+] IP 127.0.0.1 find tcp/22
[+] IP 127.0.0.1 find udp/53
```
With the params and the results obdu creates this folder struct:
```
~/.obdu/default/test/localhost/127.0.0.1/tcp/22
~/.obdu/default/test/localhost/127.0.0.1/udp/53
~/.obdu/default/test/localhost/127.0.0.1/nmap/nmapScan-udp-20210828171234.csv
~/.obdu/default/test/localhost/127.0.0.1/nmap/nmapScan-udp-20210828171234.json
```

### Hack tools Templates

Obdu work with alias associated with nmap services names on ports to link these alias with tools to generate the scripts with the know params seted.

This is a example of script template creation.

1. First of all you need create alias for a name service of nmap, this file is in config/aliases.yml
```yaml
https:
  - https
  - zeus-admin
  - snet-sensor-mgmt
```
This rule add "http" alias for services "https","zeus-admin" and "snet-sensor-mgmt" name of services extracted by nmap.
2. For these alias we need add what tools we associated, defined in file config/template.yml:
```yaml
https:
  wfuzz:
    - directory
  whatweb:
    - fingerprint
```
In these example the configuration have associated wfuzz tools.
3. The file with commands of hack tools are defined in the config/tools.yml:
```yaml
wfuzz:
  options:
    - '-t 3'
    - '--req-delay 4 -s 0.100 -t 1'
  templates:
    directory: 'wfuzz {{options}} -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" -c -Z --hc 404 -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-small.txt --oF $TOOL-$(date +"%Y%m%d%H%M%S").bin -f $TOOL-$(date +"%Y%m%d%H%M%S").log "{{subProto}}://{{domain}}:{{port}}/FUZZ"'
whatweb:
  options:
    - '-a 4'
    - '-a 1'
  templates:
    fingerprint: 'whatweb {{options}} -U "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" "{{subProto}}://{{domain}}:{{port}}"'
```
The template created use variables like "{{domain}}" or "{{subProto}}", these variables are extracted from the paramas passed in the obdu command or from the results of nmap scan.
4. To create the final script with each template exist the template:  
```jinja
#!/bin/bash
TOOL="{{tool}}.{{templateName}}"
{{command}} | tee $TOOL.$(date +"%Y%m%d%H%M%S").log
```
5. Finally, when you execute obdu and it find the ports with the aliases defined, match the aliases with tools and templates and create bash script with the command template, an example of path ~/.obdu/default/test/localhost/127.0.0.1/tcp/80/wfuzz/wfuzz.directory.sh
```bash
wfuzz -t 3 -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" -c -Z --hc 404 -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-small.txt --oF $TOOL-$(date +"%Y%m%d%H%M%S").bin -f $TOOL-$(date +"%Y%m%d%H%M%S").log "https://localhost:443/FUZZ" | tee $TOOL.$(date +"%Y%m%d%H%M%S").log
```

## Installation

### 1. Python 3 and pip installation
```bash
apt install python3.8 python3-pip
```

### 2. Install python packages with pip
```bash
pip3 install jsbeautifier pyyaml termcolor python-nmap jinja2
```

### 2. (Alternative) Install python packages with poetry
```bash
poetry install
```

## Run

### 1. Execute with python (required installation with pip.)
```
python3 obdu/obdu.py ...
```

### 1. (Alternative) Execute with poetry (required installation with poetry)
```
poetry run python3 obdu/obdu.py ...
```
